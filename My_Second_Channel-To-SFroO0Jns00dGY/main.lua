-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
function main(Data)
   -- create a new "From channel" to "To Translator" channel 
 
   -- get the default config for a "From channel" to "To Translator" channel
   local Config = net.http.post{url='localhost:6543/get_default_config',
      auth={password='password', username='admin'},
      parameters={compact='true', source='From Channel', destination='To Translator'},
      live=true
   }
   local XmlTreeCfg = xml.parse(Config)
 
   -- set the channel name
   XmlTreeCfg.channel.name = 'UC - Real Time ADT Gen Test'
 
   -- Change the source channel (source_name attribute) to "My Channel"
   local List=XmlTreeCfg.channel.to_mapper.dequeue_list
   List:append(xml.ELEMENT,'dequeue')
   List.dequeue.source_name='UC - Real Time ADT Generator'
 
   -- create the new channel
   local MyChannelConfig = net.http.post{url='localhost:6543/add_channel',
      auth={password='password', username='admin'},
      parameters={compact='true', config=tostring(XmlTreeCfg)},
      live=false
   }
 
   -- run this after you have created the channel to
   -- display the settings for the new channel
   local Config = net.http.post{url='localhost:6543/get_channel_config',
      auth={password='password', username='admin'},
      parameters={compact='true', name='UC - Real Time ADT Gen Test'},
      live=true
   }
   local XmlTreeCfg = xml.parse(Config) 
   trace(XmlTreeCfg) -- view the settings
end